package org.home.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Paper {
    private Integer id;
    private String title;
    private Set<Author> authors = new HashSet<>();
    private Journal journal;
    private LocalDate year;
    private Integer volume;
    private Integer issue;

    private PageRange pages;

    public Paper() {
    }

    public Paper(String title, Journal journal, Integer year, Integer volume, Integer issue, PageRange pages) {
        this.title = title;
        this.journal = journal;
        setYear(year);
        this.volume = volume;
        this.issue = issue;
        this.pages = pages;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public Journal getJournal() {
        return journal;
    }

    public LocalDate getYear() {
        return year;
    }

    public Integer getYearFormatted() {
        return year.getYear();
    }

    public Integer getVolume() {
        return volume;
    }

    public Integer getIssue() {
        return issue;
    }

    public PageRange getPages() {
        return pages;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public void setYear(Integer year) {
        this.year = LocalDate.of(year, 1, 1);
    }

    public void setYear(LocalDate date) {
        this.year = date;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public void setIssue(Integer issue) {
        this.issue = issue;
    }

    public void setPages(PageRange pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Paper{" +
                "paperId=" + id +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", journal='" + journal + '\'' +
                ", year=" + getYearFormatted() +
                ", volume=" + volume +
                ", issue=" + issue +
                ", pages=" + pages +
                '}';
    }


    static public class PageRange {
        private Integer pageStart;

        private Integer pageEnd;

        public PageRange() {
        }

        public PageRange(Integer pageStart, Integer pageEnd) {
            this.pageStart = pageStart;
            this.pageEnd = pageEnd;
        }

        public Integer getPageStart() {
            return pageStart;
        }

        public Integer getPageEnd() {
            return pageEnd;
        }

        public void setPageStart(Integer pageStart) {
            this.pageStart = pageStart;
        }

        public void setPageEnd(Integer pageEnd) {
            this.pageEnd = pageEnd;
        }

        @Override
        public String toString() {
            return pageEnd != null ? pageStart + "-" + pageEnd : String.valueOf(pageStart);
        }
    }
}
