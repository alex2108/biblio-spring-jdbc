package org.home.mapping;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

public class UpdateJournal extends SqlUpdate {
    private static final String QUERY = "update journal " +
            "set name = :name, edition = :edition, impact_factor = :impactFactor " +
            "where journal_id = :journalId";

    public UpdateJournal(DataSource ds) {
        super(ds, QUERY);
        super.declareParameter(new SqlParameter("name", Types.VARCHAR));
        super.declareParameter(new SqlParameter("edition", Types.VARCHAR));
        super.declareParameter(new SqlParameter("impactFactor", Types.NUMERIC));
        super.declareParameter(new SqlParameter("journalId", Types.INTEGER));
    }
}
