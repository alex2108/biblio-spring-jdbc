package org.home.mapping;

import javax.sql.DataSource;

public class SelectAllJournals extends SelectJournal {

    private static final String QUERY = "select * from journal";

    public SelectAllJournals(DataSource ds) {
        super(ds, QUERY);
    }
}
