package org.home.mapping;

import org.springframework.jdbc.core.SqlParameter;

import javax.sql.DataSource;
import java.sql.Types;

public class SelectJournalByEdition extends SelectJournal {

    private static final String QUERY = "select * from journal where edition = :edition";

    public SelectJournalByEdition(DataSource ds) {
        super(ds, QUERY);
        super.declareParameter(new SqlParameter("edition", Types.VARCHAR));
    }
}
