package org.home.mapping;

import org.springframework.jdbc.core.SqlParameter;

import javax.sql.DataSource;
import java.sql.Types;

public class SelectJournalById extends SelectJournal {

    private static final String QUERY = "select * from journal where journal_id = :journalId";

    public SelectJournalById(DataSource ds) {
        super(ds, QUERY);
        super.declareParameter(new SqlParameter("journalId", Types.INTEGER));
    }
}
