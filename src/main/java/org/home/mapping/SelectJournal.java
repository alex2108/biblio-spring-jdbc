package org.home.mapping;

import org.home.model.Journal;
import org.springframework.jdbc.object.MappingSqlQuery;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectJournal extends MappingSqlQuery<Journal> {

    public SelectJournal(DataSource ds, String query) {
        super(ds, query);
    }

    @Override
    protected Journal mapRow(ResultSet resultSet, int i) throws SQLException {
        Journal journal = new Journal();
        journal.setId(resultSet.getInt("journal_id"));
        journal.setName(resultSet.getString("name"));
        journal.setISSN(resultSet.getString("ISSN"));
        journal.setEdition(resultSet.getString("edition"));
        journal.setImpactFactor(resultSet.getBigDecimal("impact_factor"));
        return journal;
    }
}
