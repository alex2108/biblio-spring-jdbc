package org.home.daoimpl;

import org.home.model.Author;
import org.home.dao.AuthorDao;
import org.home.model.Paper;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.*;

public class AuthorDaoImpl implements AuthorDao, InitializingBean {

    private static final RowMapper<Author> authorMapper = (resultSet, rowNum) -> {
        Author author = new Author();
        author.setId(resultSet.getInt("author_id"));
        author.setFirstName(resultSet.getString("first_name"));
        author.setLastName(resultSet.getString("last_name"));
        author.setPatronymic(resultSet.getString("patronymic"));
        return author;
    };

    private static final ResultSetExtractor<List<Author>> authorsJoinPaperMapper = resSet -> {
      Map<Integer, Author> map = new HashMap<>();
        Author author;
      while (resSet.next()) {
          Integer id = resSet.getInt("author_id");
          author = map.get(id);
          if (author == null) {
              author = new Author();
              author.setId(id);
              author.setFirstName(resSet.getString("first_name"));
              author.setLastName(resSet.getString("last_name"));
              author.setPatronymic(resSet.getString("patronymic"));
              author.setPapers(new HashSet<>());
          }
          Integer paperId = resSet.getInt("paper_id");
          if (paperId > 0) {
              Paper paper = new Paper();
              paper.setId(paperId);
              paper.setTitle(resSet.getString("title"));
              paper.setYear(resSet.getDate("year").toLocalDate());
              paper.setVolume(resSet.getInt("volume"));
              paper.setIssue(resSet.getInt("issue"));
              Paper.PageRange pageRange = new Paper.PageRange();
              pageRange.setPageStart(resSet.getInt("page_start"));
              pageRange.setPageEnd(resSet.getInt("page_end"));
              paper.setPages(pageRange);
              author.getPapers().add(paper);
          }
          map.put(id, author);
      }
      return new ArrayList<>(map.values());
    };

    private JdbcTemplate jdbcTemplate;

    private NamedParameterJdbcTemplate namedJdbcTemplate;

    private SimpleJdbcInsert jdbcInsert;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setNamedJdbcTemplate(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public String findFirstNameById(Integer id) {
        return jdbcTemplate.queryForObject(
                "select first_name " +
                        "from author where author_id=?",
                new Object[]{id},
                String.class
        );
    }

    @Override
    public String findLastNameById(Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("authorId", id);
        return namedJdbcTemplate.queryForObject("select last_name " +
                        "from author where author_id = :authorId",
                params, String.class);
    }

    @Override
    public Author findById(Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("authorId", id);
        return namedJdbcTemplate.queryForObject("select * " +
                        "from author where author_id = :authorId",
                params, authorMapper);
    }

    @Override
    public List<Author> findAll() {
        return namedJdbcTemplate.query("select * from author order by author_id", authorMapper);
    }

    @Override
    public Author findByIdWithPapers(Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("authorId", id);
        String selectQuery = "select a.*, ap.author_id as ap_author_id, ap.paper_id as ap_paper_id, p.* " +
                "from author a " +
                "left join author_paper ap on a.author_id=ap.author_id " +
                "left join paper p on ap.paper_id=p.paper_id " +
                "where a.author_id = :authorId";
        List<Author> authors = namedJdbcTemplate.query(selectQuery, params, authorsJoinPaperMapper);
        return authors != null ? authors.get(0) : null;
    }

    @Override
    public List<Author> findAllWithPapers() {
        String selectQuery = "select a.*, ap.author_id as ap_author_id, ap.paper_id as ap_paper_id, p.* " +
                "from author a " +
                "left join author_paper ap on a.author_id=ap.author_id " +
                "left join paper p on ap.paper_id=p.paper_id";
        return namedJdbcTemplate.query(selectQuery, authorsJoinPaperMapper);
    }

    @Override
    public Author insert(Author author) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("firstName", author.getFirstName());
        params.addValue("lastName", author.getLastName());
        params.addValue("patronymic", author.getPatronymic());
        String insertQuery = "insert into author (author_id, first_name, last_name, patronymic) " +
                "values(seq_author.nextval, :firstName, :lastName, :patronymic)";
        KeyHolder holder = new GeneratedKeyHolder();
        namedJdbcTemplate.update(insertQuery, params, holder, new String[]{"author_id"});
        author.setId(holder.getKey().intValue());
        return author;
    }

    @Override
    public int update(Author author) {
        Map<String, Object> params = new HashMap<>();
        params.put("authorId", author.getId());
        params.put("firstName", author.getFirstName());
        params.put("lastName", author.getLastName());
        params.put("patronymic", author.getPatronymic());
        String updateQuery = "update author " +
                "set first_name = :firstName, last_name = :lastName, patronymic = :patronymic " +
                "where author_id = :authorId";
        return namedJdbcTemplate.update(updateQuery, params);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (jdbcTemplate == null || namedJdbcTemplate == null)
            throw new BeanCreationException("Must be set jdbcTemplate or namedJdbcTemplate on AuthorDao");
    }
}
