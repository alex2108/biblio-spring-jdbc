package org.home.daoimpl;

import org.home.dao.JournalDao;
import org.home.mapping.SelectAllJournals;
import org.home.mapping.SelectJournalByEdition;
import org.home.mapping.SelectJournalById;
import org.home.mapping.UpdateJournal;
import org.home.model.Journal;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JournalDaoImpl implements JournalDao, InitializingBean {

    private DataSource dataSource;

    private SelectJournalById selectJournalById;

    private SelectAllJournals selectAllJournals;

    private SelectJournalByEdition selectJournalByEdition;

    private UpdateJournal updateJournal;

    public JournalDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.selectJournalById = new SelectJournalById(dataSource);
        this.selectAllJournals = new SelectAllJournals(dataSource);
        this.selectJournalByEdition = new SelectJournalByEdition(dataSource);
        this.updateJournal = new UpdateJournal(dataSource);
    }

    @Override
    public Journal findById(Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("journalId", id);
        return selectJournalById.findObjectByNamedParam(params);
    }

    @Override
    public List<Journal> findByEdition(String edition) {
        Map<String, Object> params = new HashMap<>();
        params.put("edition", edition);
        return selectJournalByEdition.executeByNamedParam(params);
    }

    @Override
    public List<Journal> findAll() {
        return selectAllJournals.execute();
    }

    @Override
    public void update(Journal journal) {
        Map<String, Object> params = new HashMap<>();
        params.put("journalId", journal.getId());
        params.put("edition", journal.getEdition());
        params.put("name", journal.getName());
        params.put("impactFactor", journal.getImpactFactor());
        updateJournal.updateByNamedParam(params);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (selectJournalById == null || selectAllJournals == null || selectJournalByEdition == null || updateJournal == null)
            throw new BeanCreationException("Must be set mapping implementations on JournalDao");
    }
}
