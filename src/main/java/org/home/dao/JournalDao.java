package org.home.dao;

import org.home.model.Journal;

import java.util.List;

public interface JournalDao {
    Journal findById(Integer id);

    List<Journal> findByEdition(String edition);

    List<Journal> findAll();

    void update(Journal journal);
}
