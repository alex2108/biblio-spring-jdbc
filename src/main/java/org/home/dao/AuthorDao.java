package org.home.dao;

import org.home.model.Author;

import java.util.List;

public interface AuthorDao {

    String findFirstNameById(Integer id);

    String findLastNameById(Integer id);

    Author findById(Integer id);

    Author findByIdWithPapers(Integer id);

    List<Author> findAll();

    List<Author> findAllWithPapers();

    Author insert(Author author);

    int update(Author author);

}
