package org.home.daoimpl;

import org.home.dao.AuthorDao;
import org.home.model.Author;
import org.home.model.Paper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;

public class AuthorDaoImplTest {

    private GenericXmlApplicationContext ctx;
    private AuthorDao authorDao;

    @Before
    public void setUp() {
        ctx = new GenericXmlApplicationContext(); //or GenericApplicationContext for config class
        ctx.load("classpath:datasource-cfg.xml");
        ctx.refresh();
        authorDao = ctx.getBean(AuthorDao.class);
        assertNotNull(authorDao);
    }

    @Test
    public void testInsertAuthor() {
        Author author = new Author();
        author.setFirstName("Artem");
        author.setLastName("Grigorev");
        Author authorAfter = authorDao.insert(author);
        Author authorDB = authorDao.findById(authorAfter.getId());
        assertAll(
                () -> assertNotNull(authorAfter.getId()),
                () -> assertEquals("Artem", authorDB.getFirstName()),
                () -> assertEquals("Grigorev", authorDB.getLastName())
        );
    }

    @Test
    public void testFindAuthor() {
        Author author = authorDao.findById(50);
        List<Author> allAuthors = authorDao.findAll();
        assertAll(
                () -> assertEquals("Max", authorDao.findFirstNameById(16)),
                () -> assertEquals("Ivanova", authorDao.findLastNameById(11)),
                () -> assertEquals("Ivan", author.getFirstName()),
                () -> assertEquals("Stepanov", author.getLastName()),
                () -> assertEquals("Ivanovich", author.getPatronymic())
                //() -> assertEquals(17, allAuthors.size())
        );
    }

    @Test
    public void testFindAuthorsWithPapers() {
        List<Author> allAuthorsWithPapers = authorDao.findAllWithPapers();
        Author author = allAuthorsWithPapers.stream()
                .filter(entity -> entity.getId().equals(20)).findAny().orElse(null);
        assertAll(
                //() -> assertEquals(17, allAuthorsWithPapers.size()),
                () -> assertNotNull(author),
                () -> assertEquals("Yulia", author.getFirstName()),
                () -> assertEquals(2, author.getPapers().size()),
                () -> assertTrue(author.getPapers().stream().map(Paper::getTitle).collect(Collectors.toList()).contains("Exciting paper"))
        );
    }

    @Test
    public void testUpdate() {
        Author author = authorDao.findAll().get(0);
        String firstName = author.getFirstName();
        author.setFirstName(firstName + "123");
        int res = authorDao.update(author);
        assertAll(
                () -> assertEquals(1, res),
                () -> assertEquals(firstName + "123", authorDao.findById(author.getId()).getFirstName()),
                () -> assertEquals(author.getLastName(), authorDao.findById(author.getId()).getLastName())
        );
    }

    @After
    public void closeCtx() {
        ctx.close();
    }

}
