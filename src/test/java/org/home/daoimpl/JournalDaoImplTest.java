package org.home.daoimpl;

import org.home.dao.JournalDao;
import org.home.model.Journal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;

public class JournalDaoImplTest {

    private GenericXmlApplicationContext ctx;
    private JournalDao journalDao;

    @Before
    public void setUp() {
        ctx = new GenericXmlApplicationContext(); //or GenericApplicationContext for config class
        ctx.load("classpath:datasource-cfg.xml");
        ctx.refresh();
        journalDao = ctx.getBean(JournalDao.class);
        assertNotNull(journalDao);
    }

    @Test
    public void testJournalUpdate() {
        //find and update
        Journal journal = journalDao.findById(8);
        journal.setEdition("Apress");
        journal.setImpactFactor(new BigDecimal("1.5"));
        journalDao.update(journal);

        //check
        Journal updatedJournal = journalDao.findById(8);
        assertAll(
                () -> assertEquals("Apress", updatedJournal.getEdition()),
                () -> assertEquals(new BigDecimal("1.5"), updatedJournal.getImpactFactor())
        );
    }

    @Test
    public void testFindAll() {
        List<Journal> journals = journalDao.findAll();
        assertAll(
                () -> assertEquals(4, journals.size()),
                () -> assertTrue(Arrays.asList("Lecture of Computer Science", "European Journal of OR", "Hello world", "Artificial Intelligence").
                        containsAll(journals.stream().map(Journal::getName).collect(Collectors.toList())))
        );
    }

    @Test
    public void testFindByEdition() {
        List<Journal> journals = journalDao.findByEdition("Elsevier");
        assertAll(
                () -> assertEquals(3, journals.size()),
                () -> assertTrue(Arrays.asList("Lecture of Computer Science", "European Journal of OR", "Artificial Intelligence").
                        containsAll(journals.stream().map(Journal::getName).collect(Collectors.toList())))
        );
    }

    @After
    public void closeCtx() {
        ctx.close();
    }

}
